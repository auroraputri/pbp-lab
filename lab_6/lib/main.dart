import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:lab_6/screens/kasi_review.dart';


void main() {
  // debugPaintSizeEnabled = true;
  runApp(MaterialApp(
    home: Homemed()));
}



class Homemed extends StatelessWidget {
  const Homemed({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(50),
      child: Wrap(
        children: [
          Image.asset(
              'images/kn95.jpg',
              width: 300,
              height: 300,
              fit: BoxFit.contain,
              
            ),
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 20, left: 20),
                  child: Column(
                    children: [
                      const Text(
                        'Masker KN95',
                        style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 30,
                        ),
                      ),

                      AutoSizeText(
                        'Masker Non-Medis',
                        // semanticsLabel: 'Masker dengan kemampuan menyaring udara hingga 95 persen dan Kulitas yang sangat baik',
                        style: TextStyle(
                          color: Colors.blue[700],
                          fontWeight: FontWeight.w600,
                          
                        ),
                        minFontSize: 15,
                        // textAlign: TextAlign.left,
                      ),
                    ]
                  )
                ),

                Container(
                  padding: const EdgeInsets.only(bottom: 20, left: 20),
                  child: const Text(
                    'RP25.000',
                    style: TextStyle(
                      fontWeight: FontWeight.w700,
                      fontSize: 40,
                      color: Colors.teal,
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.only(bottom: 20, left: 20),
                  child: Wrap(
                    children: [
                      Icon(
                        Icons.shopping_cart_outlined,
                        color: Colors.blue[600],
                        ),
                      const Text(
                        'Buy Now',
                        ),
                    ],
                  )
                ),
                Container(
                  padding: const EdgeInsets.only(bottom: 20, left: 20),
                  child: const AutoSizeText(
                      'Masker dengan kemampuan menyaring udara hingga 95 persen dan Kulitas yang sangat baik',
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                      minFontSize: 12,
                      // textAlign: TextAlign.left,
                    ),
                ),
                
              ],
            ),
          ),
        ],
      ),
    );

    Widget buttonSection = CupertinoPageScaffold(
      navigationBar: const CupertinoNavigationBar(
        automaticallyImplyLeading: false,
        middle:
            AutoSizeText("Penilaian",
            style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w600,
                    fontSize: 24,
                  ),
            minFontSize: 12,
            ),
      ),
      // child:(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CupertinoButton(
              onPressed: () {},
              child: Text(""),
            ),
            const SizedBox(height: 16),
            CupertinoButton.filled(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return kasi_review();
                    },
                  ),
                );
              },
              child: Text("Add Review"),
            ),
          ],
        ),
    );


    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'HomeMed',
      home: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Icon(Icons.chat),
        ),
        appBar: AppBar(
          title: const Text('HomeMed'),
        ),
        body: ListView(
          children: [
            titleSection,
            buttonSection,
          ],
        ),
      ),
    );
  }
}