from django.urls import path
from .views import add_note, index, note_list

urlpatterns = [
    path('', index, name='index'),  #menambah path untuk halaman utama lab-4
    path('add-note', add_note),    #path untuk menambah note kedalam daftar
    path('note-list', note_list),   #path untuk menampilkan note dalam bentuk card
]
