from django.shortcuts import render
from lab_2.models import Note
from .form import NoteForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect

# Create your views here.
@login_required(login_url = '/admin/login/')
def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

@login_required(login_url = '/admin/login/')
def add_note(request):
    form = NoteForm(request.POST or None)     #untuk memanggil request yang diminta pada class NoteForm
    if request.method == 'POST':
        if form.is_valid():             #kalau semua sudah ada isi yang sesuai
            form.save()
        return HttpResponseRedirect('/lab-4')   #mendirect kembali ke lab-4
    response = {'form': form} 
    return render(request, 'lab4_form.html', response) 

@login_required(login_url = '/admin/login/')
def note_list(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab4_note_list.html', response)