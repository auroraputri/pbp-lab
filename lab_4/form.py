from django.forms.widgets import NumberInput
from lab_2.models import Note
from django import forms

# membuat class FriendForm sesuai instruksi
class NoteForm(forms.ModelForm):
	class Meta:
            model = Note      
            fields = ['untuk', 'dari', 'judul', 'pesan']
