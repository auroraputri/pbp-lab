// ignore_for_file: prefer_const_constructors, prefer_const_declarations

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:lab_7/main.dart';
// import 'package:lab_6/button.dart';

// Future main() async {
//   WidgetsFlutterBinding.ensureInitialized();
//   await SystemChrome.setPreferredOrientations([
//     DeviceOrientation.portraitUp,
//     DeviceOrientation.portraitDown,
//   ]);

//   runApp(MyApp());
// }

class kasi_review extends StatelessWidget {
  static String title = 'Customer Support';

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    title: title,
    theme: ThemeData(primaryColor: Colors.teal),
    home: MainPage(title: title),
  );
}

class MainPage extends StatefulWidget {
  final String title;

  const MainPage({
    required this.title,
  });

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final formKey = GlobalKey<FormState>();
  String nama = '';
  String penilaian = '';

  Widget Judul() => Text(
        "Berikan Review Mengenai Produk Kami",
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 24, fontWeight: FontWeight.w500, color: Colors.teal[900])
      );

  //Referensi : https://github.com/boriszv/Programming-Addict-Code-Examples/blob/master/flutter_forms/lib/main.dart
  @override
  Widget build(BuildContext context) => Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Icon(Icons.chat),
        ),
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Form(
          key: formKey,
          child: ListView(
            padding: EdgeInsets.all(16),
            children: [
              Judul(),
              const SizedBox(height: 16),
              buildNama(),
              const SizedBox(height: 16),
              reviewNya(),
              const SizedBox(height: 32),
              buildSubmit(),
            ],
          ),
        ),
      );

  Widget buildNama() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Nama',
          border: OutlineInputBorder(),
        ),
        maxLength: 50,
        validator: (value) {
          if (value!.isEmpty) {
            return 'Masukan Nama';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => nama = value!),
      );

  Widget reviewNya() => TextFormField(
        decoration: InputDecoration(
          labelText: 'Penilaian',
          border: OutlineInputBorder(),
        ),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Masukan Penilaian';
          } else {
            return null;
          }
        },
        onSaved: (value) => setState(() => penilaian = value!),
      );

  Widget buildSubmit() => TextButton(
        child: Text("Add Review"),
        onPressed: () {
          final isValid = formKey.currentState!.validate();

          if (isValid) {
            formKey.currentState!.save();

            final message = 'Dear $nama, terimakasih telah menambahkan penialian';
            print(nama);
            print(penilaian);
            print(message);

            final snackBar = SnackBar(
              content: Text(
                message,
                style: TextStyle(fontSize: 20),

              ),
              backgroundColor: Colors.teal[600],
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }

          Future.delayed(Duration(seconds: 7), () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) {
                  return Homemed();
                },
              ),
            );
          });
          
        },
        style: TextButton.styleFrom(
            primary: Colors.black, backgroundColor: Colors.teal[100]
            ),
      );
}
