from django.db import models

# TODO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30, default= None)
    # TODO Implement missing attributes in Friend model
    ''''
    Pada class friend dibutuhkan atribut npm dan date of birth di mana 
    nantinya kita akan memasukan data tersebut secara manual pada web
    '''
    npm = models.IntegerField(default= None)       
    dob = models.DateField( default= None)
