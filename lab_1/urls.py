from django.urls import path
from .views import index, friend_list   #import fungsi index dan friend_list dari file views

urlpatterns = [
    path('', index, name='index'),
    # TODO Add friends path using friend_list Views
    path('friends', friend_list, name='friend_list'),   #menambahkan path dengan route friends dari fungsi friend_list
]
