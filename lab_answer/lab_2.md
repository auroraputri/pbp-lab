# 1. Apakah perbedaan antara JSON dan XML?
Hal pertama yang saya tangkap dari perbedaan antara JSON dan XML adalah berdasarkan cara merepresentasikannya. XML sendiri merepresentasikan data dengan struktur tag dan bahasa mark up dan JSON menggunakan cara seperti list yang berisi dictionaries. Lalu JSON sendiri memiliki ukuran file yang lebih kecil dibanding XML. Karena ukurannya yang lebih kecil, JSON juga lebih cepat dalam memproses dan transfer data. JSON juga tujukan untuk menyimpan data lebih terorganisir dan mudah dipahami. XML sendiri sangat bisa untuk menyimpan data yang ukurannya besar seperti metadata dan data yang rumit. Mungkin jika kita bisa mengambir garis besarnya adalah JSON lebih sederhana dan ringan dibanding XML, tetapi XML lebih kuat daripada JSON

> Untuk lebih detailnya bisa melihat sumber [di sini](https://www.guru99.com/json-vs-xml-difference.html)
# 2. Apakah perbedaan antara HTML dan XML?
Berdasarpada hasil kelas sinkronus dan asikronus, saya menagngkap bahwa HTML berfokus dalam penyajian data dan XML berfokus pada pemrosesan data (bisa berupa transfer data). XML juga memberikan kita kemudahan untuk melakukan transfer data antar database dan aplikasi. Berikut sajian perbedaan HTML dan XML

- XML is content driven whereas HTML is format driven.
- XML is Case sensitive while HTML is Case insensitive.
- XML provides namespaces support while HTML doesn’t provide namespaces support.
- XML is strict for closing tag while HTML is not strict.
- XML tags are extensible whereas HTML has limited tags.
- XML tags are not predefined whereas HTML has predefined tags.

> Untuk lebih detailnya bisa melihat sumber [di sini](https://www.guru99.com/xml-vs-html-difference.html)




