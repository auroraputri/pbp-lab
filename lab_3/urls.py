from django.urls import path
from .views import index, add_friend

urlpatterns = [
    path('', index, name='index'),  #menambah path untuk halaman utama lab-3
    path('add', add_friend),    #path untuk menambah person kedalam daftar
]
