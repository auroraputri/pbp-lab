from django.shortcuts import redirect, render
from lab_1.models import Friend
from .form import FriendForm
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect


# Create your views here.
@login_required(login_url = '/admin/login/')    #agar yang mengoperasikan aplikasi hanya yangterdaftar sebagai admin
def index(request):
    friends = Friend.objects.all().values()  # TODO Implement this
    response = {'friends': friends}
    return render(request, 'index_lab3.html', response)

@login_required(login_url = '/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)     #untuk memanggil request yang diminta pada class friendform
    if request.method == 'POST':
        if form.is_valid():             #kalau semua sudah ada isi yang sesuai
            form.save()
        return HttpResponseRedirect('/lab-3')   #mendirect kembali ke lab-3
    response = {'form': form} 
    return render(request, 'lab3_form.html', response)   
