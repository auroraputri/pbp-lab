from django.forms.widgets import NumberInput
from lab_1.models import Friend
from django import forms

# membuat class FriendForm sesuai instruksi
class FriendForm(forms.ModelForm):
	class Meta:
            model = Friend      
            fields = ['name', 'npm', 'dob']
            widgets = {'dob': NumberInput(attrs={'type': 'date'})}
