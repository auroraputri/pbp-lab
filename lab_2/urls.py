from django.urls import path
from .views import index, json, xml

''''
    Menambahkan route xml dan json pada path dan sesuai hasil yang telah di import dari views.py
'''
urlpatterns = [
    path('', index, name='index'),
    path('xml', xml),
    path('json', json),
]