from django.db import models

''''
    Pada class Note dibutuhkan atribut to, from, title, message di mana 
    nantinya kita akan memasukan data tersebut secara manual pada web
'''
class Note(models.Model):
    untuk = models.CharField(max_length=30, default= None)
    dari = models.CharField(max_length=30, default= None)
    judul = models.CharField(max_length=100, default= None)
    pesan = models.TextField()      #untuk isi pesan memakai text field untuk antisipasi pesan yang panjang
