from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from .models import Note

''''
    Menambahkan fungsi index, xml, dan json sesuai instruksi yang diberikan 
'''
#akan langsung menampilkan halaman yang berisi notes dengan nilai yang telah di input pada admin
def index(request):
    notes = Note.objects.all().values()  
    response = {'notes': notes}
    return render(request, 'lab2.html', response)

#akan langsung menampilkan halaman yang berisi XML dari values notes
def xml(request):
    notes = Note.objects.all().values()
    data = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(data, content_type="application/xml")  

#akan langsung menampilkan halaman yang berisi JSON dari values notes
def json(request):
    notes = Note.objects.all().values()
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type="application/json")

